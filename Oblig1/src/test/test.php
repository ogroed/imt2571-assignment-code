<?php

    echo "<!DOCTYPE html>
    <html>
    <head>
    </head>
    <body>";

    $db = new PDO('mysql:host=localhost;dbname=imt2571;charset=utf8', 'root', '');
    $book = $db->prepare("SELECT * FROM book WHERE id = -99");
    $book->execute();
    $book = $book->fetchAll(PDO::FETCH_OBJ);
    $book = $book[0];
    echo gettype($book);

    echo "</body></html>";

    function test_getby_id($db){
        $id = 3;
        $query = $db->query("SELECT * FROM book WHERE id = '".$id."'");
        $query = $query->fetchAll(PDO::FETCH_OBJ);
        $query = $query[0];
        echo $query->author;
    }

    function test_query($db){
        $stmt = $db->query('SELECT * FROM book');
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        echo $result[0]->author;
    }

    function test_insert($db){
        $stmt = $db->prepare("INSERT INTO book (id, title, author, description) VALUES (:id, :title, :author, :description)");
        $stmt->execute(array(
            "id" => NULL,
            "title" => "test title",
            "author" => "test author",
            "description" => "test description"
        ));
    }

    function test_update($db){
        $stmt = $db->prepare("UPDATE book SET title = ?, author = ?, description = ? WHERE id = ?");
        $stmt->execute(array("new title", "new author", "new desc.", 5));
    }

?>
<?php

include_once("Book.php");
include_once("IModel.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class Model implements IModel
{								  
    /**
	 * @todo The session array for storing book collection is to be replaced by a database.
	 */

	protected $db = null;

	public function __construct($db = null)  
    {  
	    if($db){
			$this->db = $db;
		}else{
			try{
				$this->db = new PDO('mysql:host=localhost;dbname=imt2571;charset=utf8', 'root', '');
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}catch(PDOException $ex){
				$view = new ErrorView();
				$view->create();
			}
		}
	}
	
	/** Function returning the complete list of books in the collection. Books are
	 * returned in order of id.
	 * @return Book[] An array of book objects indexed and ordered by their id.
	 * @todo Replace implementation using a real database.
	 */
	public function getBookList()
	{
		$booklist = array();
		$booklist = $this->db->prepare('SELECT * FROM book');		
		try{
			$booklist->execute();
			$booklist = $booklist->fetchAll(PDO::FETCH_OBJ);		
		}catch(PDOException $ex){
			$view = new ErrorView();
			$view->create();
		}
		return $booklist;
	}
	
	/** Function retrieveing information about a given book in the collection.
	 * @param integer $id The id of the book to be retrieved.
	 * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @todo Replace implementation using a real database.
	 */
	public function getBookById($id)
	{
		$book = null;
		if( !filter_var($id, FILTER_VALIDATE_INT)){
			return null;
		}

		$book = $this->db->prepare("SELECT * FROM book WHERE id = $id");		
		try{
			$book->execute();
			$book = $book->fetchAll(PDO::FETCH_OBJ);
			$book = $book[0];
		}catch(PDOException $ex){
			$view = new ErrorView();
			$view->create();
		}
		
		return ($book != null) ? $book : null;

	}
	
	/** Adds a new book to the collection.
	 * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @todo Replace implementation using a real database.
	 */
	public function addBook($book)
	{
		
		if ($book->author == "" || $book->title == ""){
			$view = new ErrorView("Book title or author is empty, please fill it in");
			$view->create();
		}else{
			$stmt = $this->db->prepare("INSERT INTO book (id, title, author, description) VALUES (:id, :title, :author, :description)");
			try{
				$stmt->execute(array(
					"id" => NULL,
					"title" => $book->title,
					"author" => $book->author,
					"description" => $book->description
				));
			}catch(PDOException $ex){
				$view = new ErrorView();
				$view->create();
			}
		}

	}

	/** Modifies data related to a book in the collection.
	 * @param Book $book The book data to kept.
	 * @todo Replace implementation using a real database.
	 */
	public function modifyBook($book)
	{
		if ($book->author == "" || $book->title == ""){
			$view = new ErrorView("Book title or author is empty, please fill it in");
			$view->create();	
		}else{
			$stmt = $this->db->prepare("UPDATE book SET title = ?, author = ?, description = ? WHERE id = ?");
			try{
				$stmt->execute(array(
					$book->title, $book->author, 
					$book->description, $book->id
				));
			}catch(PDOException $ex){
				$view = new ErrorView();
				$view->create();
			}
		}

	}

	/** Deletes data related to a book from the collection.
	 * @param integer $id The id of the book that should be removed from the collection.
	 * @todo Replace implementation using a real database.
	 */
	public function deleteBook($id)
	{
		
		$stmt = $this->db->prepare("DELETE FROM book WHERE id=$id");
		try{
			$stmt->execute();
		}catch(PDOException $ex){
			$view = new ErrorView();
			$view->create();
		}

	}

}

?>